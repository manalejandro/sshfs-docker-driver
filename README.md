# sshfs docker driver

## docker driver to mount and share volumes over sshd service

## Requirements
```
docker, git, sudo and ssh
```

## Install
```
./sshfs-docker-driver.sh
```

## docker-compose volume config example with some options
```
volumes:
  ssh-nginx:
    driver: vieux/sshfs
    driver_opts:
      sshcmd: user@host:/var/www/html
      password: p4ssw0rd
      port: 22
#      sftp_server: "/usr/bin/sudo /usr/lib/openssh/sftp-server"
      allow_other: ""
      reconnect: ""
      StrictHostKeyChecking: "no"
```

## add volume in docker-compose service config
```
volumes:
  - ssh-nginx:/var/www/html
```

## License
```
MIT
```