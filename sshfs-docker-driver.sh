#!/bin/bash
git clone --depth 1 https://github.com/vieux/docker-volume-sshfs
cd docker-volume-sshfs/
docker build -t rootfsimage .
cd ..
mkdir -p docker-volume-sshfs/rootfs
id=$(docker create rootfsimage true)
sudo docker export "$id" | sudo tar -x -C docker-volume-sshfs/rootfs
docker rm -vf "$id"
docker rmi rootfsimage
sudo docker plugin create vieux/sshfs docker-volume-sshfs/
docker plugin enable vieux/sshfs
docker plugin ls